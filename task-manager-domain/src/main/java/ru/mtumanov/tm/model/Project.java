package ru.mtumanov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.model.IWBS;
import ru.mtumanov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Project extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @Nullable
    @Column(nullable = true)
    private String description = "";

    @NotNull
    @Column(nullable = false)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false)
    private Date created = new Date();

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    public Project(@NotNull final String name, @NotNull final String description, @NotNull final Status status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @Override
    public String toString() {
        return "ID:" + id + " " +
                "NAME:" + name + " " +
                "DESCRIPTION:" + description + " " +
                "STATUS:" + getStatus().getDisplayName() + " " +
                "USER ID: " + getUser().id + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Project)) {
            return false;
        }
        Project project = (Project) o;
        return Objects.equals(name, project.name)
                && Objects.equals(description, project.description)
                && Objects.equals(status, project.status)
                && Objects.equals(created, project.created)
                && super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, status, created);
    }

}