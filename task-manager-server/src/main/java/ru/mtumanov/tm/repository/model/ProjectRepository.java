package ru.mtumanov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.model.IProjectRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    private static final String USER_ID = "userId";

    @NotNull
    private static final String ID = "id";


    public ProjectRepository(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<Project> projects = findAll(userId);
        for (@NotNull final Project project : projects) {
            remove(project);
        }
    }

    @Override
    @NotNull
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Project p WHERE p.userId = :userId", Project.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @Override
    @NotNull
    public List<Project> findAll(@NotNull final String userId, @NotNull final Comparator<Project> comparator) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Project p WHERE p.userId = :userId ORDER BY p." + getComporator(comparator), Project.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @Override
    @Nullable
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Project p WHERE p.userId = :userId AND p.id = :id", Project.class)
                .setParameter(USER_ID, userId)
                .setParameter(ID, id)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public long getSize(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("SELECT COUNT(1) FROM Project p WHERE p.userId = :userId", Long.class)
                .setParameter(USER_ID, userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Project project = findOneById(userId, id);
        remove(project);
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public void clear() {
        @NotNull final List<Project> projects = findAll();
        for (@NotNull final Project project : projects) {
            remove(project);
        }
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Project p", Project.class)
                .getResultList();
    }


    @Override
    @NotNull
    public List<Project> findAll(@NotNull final Comparator<Project> comparator) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Project p ORDER BY p." + getComporator(comparator), Project.class)
                .getResultList();
    }

    @Override
    @Nullable
    public Project findOneById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.find(Project.class, id);
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("SELECT COUNT(1) FROM Project p", Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final Project project = findOneById(id);
        remove(project);
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

}
